import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreatorHtml {
    private static CreatorHtml creatorHtml;

    private CreatorHtml() {
    }

    public static CreatorHtml getCreatorHtml() {
        creatorHtml = (creatorHtml == null) ? new CreatorHtml() : creatorHtml;
        return creatorHtml;
    }

    public void executor(String command, PrintWriter servletOut)  throws Exception {
        String[] split = command.split("/");

        if (split[split.length-1].equals("getdate")) {
            serveDate(command, servletOut);
        } else if ((split[1].equals("add") || (split[1].equals("mult")))) {
            mathOperation(command, servletOut);
        } else if (split[split.length - 1].equals("search")) {
            serveSearch(command, servletOut);
        } else {
            serve404(servletOut);
        }
    }

    private void serveSearch(String command, PrintWriter servletOut) throws  Exception {
        Pattern pattern = Pattern.compile("^(/\\w).+(/search)$");
        Matcher matcher = pattern.matcher(command);
        if (!matcher.find())
            throw new Exception();
       // String[] split = command.split("/");

        String path = null;
        pattern = Pattern.compile("ht.+?(?=/search)");
        matcher = pattern.matcher(command);
        while(matcher.find())
            path =matcher.group();
        servletOut.write(" <form action=\" https://google.com/search\" method=\"get\"\">\n" +
                "<input type=\"hidden\" name=\"sitesearch\" value=\""+path+"\" />" +
                "            <input type=\"search\" name=\"q\">\n" +
                "            <input type=\"submit\" value=\"Найти!\">\n" +
                "      </form>");
        servletOut.close();
    }

    private void serveDate(String command, PrintWriter servletOut) throws  Exception{
            Date date = new Date();
            String name = date.toString();
            String[] split = name.split(" ");
            servletOut.write("<h1 style=\"color:rgb(200,100,10)\">"+name+"</h1>");
            servletOut.close();
    }

    private void mathOperation(String command, PrintWriter servletOut) throws  Exception{
        Pattern pattern = Pattern.compile("^/(add|mult)/\\d/\\d$");
        Matcher matcher = pattern.matcher(command);
        if (!matcher.find()) {
            throw new Exception();
        }

        int result;

        String[] split = command.split("/");
        if (split[1].equals("add")) {
            result = Integer.parseInt(split[2]) + Integer.parseInt(split[3]);
        } else {
            result = Integer.parseInt(split[2]) * Integer.parseInt(split[3]);
        }
        servletOut.write(String.valueOf(result));
        servletOut.close();
    }

    private void serve404(PrintWriter servletOut) {
        servletOut.write("Страница не найдена :( ");
        servletOut.close();
    }
}