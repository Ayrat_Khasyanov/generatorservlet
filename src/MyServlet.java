import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/*")
public class MyServlet extends HttpServlet {
    private CreatorHtml generator;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");
        doGet(request,response);
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        response.setContentType("text/html; charset=utf8");
        PrintWriter out = response.getWriter();

        try {
            generator.executor(request.getRequestURI(), out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void init(){
        generator = CreatorHtml.getCreatorHtml();
    }
}
